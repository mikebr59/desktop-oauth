﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevDefined.OAuth.Consumer;
using DevDefined.OAuth.Framework;

namespace OAuthDesktop
{
    public partial class Form1 : Form
    {
        // Callback URL
        // You must provide this simple page to parse and display the query string parameters
        //   "oauth_verifier" and "realmId"
        // User will enter those values into this app
        private string oauthCallbackUrl = "http://example.com/Auth/Verify";

        // OAuth consumer key and secret from OAuth provider
        private string consumerKey = "myConsumerKey";
        private string consumerSecret = "myConsumerSecret";
        
        // URLs for the OAuth provider
        private string baseAuthUrl = "https://workplace.intuit.com/Connect/Begin";
        private string requestTokenUrl = "https://oauth.intuit.com/oauth/v1/get_request_token";
        private string oauthUrl = "https://oauth.intuit.com/oauth/v1";
        private string accessTokenUrl = "https://oauth.intuit.com/oauth/v1/get_access_token";

        private IToken requestToken;

        public Form1()
        {
            InitializeComponent();
        }

        private void btnBegin_Click(object sender, EventArgs e)
        {
            // Get a request token from Intuit
            IOAuthSession session = CreateSession();
            IToken theToken = session.GetRequestToken();

            requestToken = theToken;

            // Redirect user to Intuit for Auth
            string token = theToken.Token;
            string secret = theToken.TokenSecret;   // TBD - How is this used here?
            System.Diagnostics.Process.Start(
                baseAuthUrl
                    + "?oauth_token=" + token
                    + "&oauth_callback=" + UriUtility.UrlEncode(oauthCallbackUrl)
                    );
        }

        private void btnComplete_Click(object sender, EventArgs e)
        {
            // Retrieve the request token saved earlier
            IToken theToken = requestToken;

            // Use the request token and auth verifier to get an access token
            IOAuthSession clientSession = CreateSession();
            try
            {
                IToken accessToken = clientSession.ExchangeRequestTokenForAccessToken(theToken, txtVerifier.Text);
                // To Do: Store accessToken and txtRealmId.Text somewhere
                lblToken.Text = accessToken.Token;
                lblSecret.Text = accessToken.TokenSecret;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private IOAuthSession CreateSession()
        {
            OAuthConsumerContext consumerContext = new OAuthConsumerContext
            {
                ConsumerKey = consumerKey,
                ConsumerSecret = consumerSecret,
                SignatureMethod = SignatureMethod.HmacSha1
            };
            return new OAuthSession(consumerContext,
                    requestTokenUrl,
                    oauthUrl,
                    accessTokenUrl);
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}